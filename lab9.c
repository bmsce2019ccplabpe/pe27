#include<stdio.h>
void swap(int *p,int *q)
{
	int t;
	t=*p;
	*p=*q;
	*q=t;
}
int main()
{
	int p,q;
	printf("Enter the values of number 1\n");
	scanf("%d",&p);
	printf("Enter the values of number 2\n");
	scanf("%d",&q);
	swap(&p,&q);
	printf("The numbers after swapping is %d %d",p,q);
	return 0;
}